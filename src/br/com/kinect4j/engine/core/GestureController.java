package br.com.kinect4j.engine.core;

import com.primesense.nite.UserData;

import br.com.kinect4j.engine.core.listener.GestureObservable;
import br.com.kinect4j.view.Kinect4jObserver;

public interface GestureController {

	void updateAllGestures(UserData user);

	<T extends Enum<T>> void addGesture(Kinect4jObserver subject, T type, Pose... gestureParts);

	<T extends Enum<T>> void addGesture(Kinect4jObserver subject, T type, KeyFrame... gestureParts);

	boolean removeGesture(GestureObservable gesture);

	void resetGesture(GestureObservable gesture);

	void resetAllGestures();
}
