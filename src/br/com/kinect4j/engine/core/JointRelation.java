package br.com.kinect4j.engine.core;

public enum JointRelation {
	ABOVE, BELOW, LEFT_OF, RIGHT_OF, CLOSER_TO, FAR_AWAY, EQUALS_HORIZONTAL, EQUALS_VERTICAL;

}
