package br.com.kinect4j.engine.core;

public enum GestureResult {
	FAIL, SUCCEED, PAUSING;

	public int getValue() {
		switch (this) {
		case FAIL:
			return 1;
		case SUCCEED:
			return 2;
		case PAUSING:
			return 3;
		default:
			return 0;
		}
	}
}
