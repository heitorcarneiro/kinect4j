package br.com.kinect4j.engine.core;

import com.primesense.nite.UserData;

import br.com.kinect4j.engine.core.listener.PoseObservable;
import br.com.kinect4j.view.Kinect4jObserver;

public interface PoseController {
	void updateAllPoses(UserData user);

	<T extends Enum<T>> void addPose(T name, int poseNumberFrames, Pose pose, Kinect4jObserver subject);

	boolean removePose(PoseObservable pose);

	<T extends Enum<T>> Integer getProgressPercentage(T name);
}
