package br.com.kinect4j.engine.core;

public class KeyFrame {
	private Pose pose;
	private int inferiorLimitFrame;
	private int superiorLimitFrame;

	public KeyFrame(Pose pose, int inferiorLimitFrame, int superiorLimitFrame) {
		this.pose = pose;
		this.inferiorLimitFrame = inferiorLimitFrame;
		this.superiorLimitFrame = superiorLimitFrame;
	}

	public Pose getPose() {
		return pose;
	}

	public int getInferiorLimitFrame() {
		return inferiorLimitFrame;
	}

	public int getSuperiorLimitFrame() {
		return superiorLimitFrame;
	}

}
