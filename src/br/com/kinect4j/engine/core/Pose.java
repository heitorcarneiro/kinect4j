package br.com.kinect4j.engine.core;

import com.primesense.nite.UserData;

public interface Pose {
	GestureResult checkPose(UserData user);
}
