package br.com.kinect4j.engine.core;

import java.util.HashMap;

import com.primesense.nite.JointType;
import com.primesense.nite.Point2D;
import com.primesense.nite.Point3D;
import com.primesense.nite.SkeletonJoint;
import com.primesense.nite.UserData;

public interface Skeleton {
	SkeletonJoint getSkeletonJoint(UserData user, JointType type);

	Point2D<Float> getPosition2D(SkeletonJoint skeletonJoint);

	Point3D<Float> getPosition3D(SkeletonJoint skeletonJoint);

	Point3D<Float> getPosition3D(UserData user, JointType type);

	Point3D<Float> getPosition3D(HashMap<JointType, SkeletonJoint> skel, JointType type);

	float getPositionX(UserData user, JointType type);

	float getPositionY(UserData user, JointType type);

	float getPositionZ(UserData user, JointType type);

	boolean checkSkeletonPosition(UserData user, JointType from, JointRelation relation, JointType to);

	/**
	 * Compare with: equals sign (=), greater than (>) or less than (<).
	 * 
	 * @author Heitor
	 */
	public enum Operator {
		EQUALS, GREATER_THAN, LESS_THAN;
	}

	boolean comparePositionX(UserData user, JointType from, Operator relation, JointType to);

	boolean comparePositionY(UserData user, JointType from, Operator relation, JointType to);

	boolean comparePositionZ(UserData user, JointType from, Operator relation, JointType to);

	double getMarginError();

	/**
	 * Default methods
	 * 
	 * @param p1
	 * @param p2
	 * @return
	 */

	default double calculateEuclideanDistance3D(Point3D<Float> p1, Point3D<Float> p2)
	// the Euclidean distance between the two points
	{
		return Math.sqrt(Math.pow((p1.getX() - p2.getX()), 2) + Math.pow((p1.getY() - p2.getY()), 2)
				+ Math.pow((p1.getZ() - p2.getZ()), 2));
	}

	default double calculateEuclideanDistance2D(Point2D<Float> p1, Point2D<Float> p2)
	// the Euclidean distance between the two points
	{
		return Math.sqrt(Math.pow((p1.getX() - p2.getX()), 2) + Math.pow((p1.getY() - p2.getY()), 2));

	}

	default boolean compareWithMarginError(double marginError, double value1, double value2) {
		return (value1 >= (value2 - marginError)) && (value1 <= (value2 + marginError));
	}

	default boolean comparePosition(float positionFrom, Operator relation, float positionTo) {
		boolean result = false;

		switch (relation) {
		case EQUALS:
			result = (compareWithMarginError(getMarginError(), positionFrom, positionTo));
			break;
		case GREATER_THAN:
			result = (positionFrom > positionTo);
			break;
		case LESS_THAN:
			result = (positionFrom < positionTo);
			break;
		}

		return result;
	}

	default double calculateScalarProduct(SkeletonJoint joint1, SkeletonJoint joint2) {
		org.openni.Point3D<Double> v = createVectorBetweenTwoPoints(joint1, joint2);
		org.openni.Point3D<Double> w = createVectorBetweenTwoPoints(joint1, joint2);

		double resultRadians = Math.acos(calculateVectorsProduct(v, w) / calculateVectorsModule(v, w));

		double resultDegrees = Math.toDegrees(resultRadians); // resultadoRadianos
																// * 180 /
																// Math.PI

		return resultDegrees;
	}

	default double calculateVectorsProduct(org.openni.Point3D<Double> p1, org.openni.Point3D<Double> p2) {
		return (p1.getX() * p2.getX()) + (p1.getY() * p2.getY()) + (p1.getZ() * p2.getZ());
	}

	default double calculateVectorsModule(org.openni.Point3D<Double> p1, org.openni.Point3D<Double> p2) {
		return (Math.sqrt(Math.pow(p1.getX(), 2) + Math.pow(p1.getY(), 2) + Math.pow(p1.getZ(), 2)))
				* (Math.sqrt(Math.pow(p2.getX(), 2) + Math.pow(p2.getY(), 2) + Math.pow(p2.getZ(), 2)));
	}

	default org.openni.Point3D<Double> createVectorBetweenTwoPoints(SkeletonJoint joint1, SkeletonJoint joint2) {
		double x = Math.sqrt(Math.pow((joint1.getPosition().getX() - joint2.getPosition().getX()), 2));
		double y = Math.sqrt(Math.pow((joint1.getPosition().getY() - joint2.getPosition().getY()), 2));
		double z = Math.sqrt(Math.pow((joint1.getPosition().getZ() - joint2.getPosition().getZ()), 2));
		return new org.openni.Point3D<Double>(x, y, z);
	}

	/**
	 * Simple info methods
	 * 
	 * @param user
	 * @param type
	 * @return
	 */

	default String getPoint2DJson(UserData user, JointType type) {
		StringBuffer json = new StringBuffer();
		Point2D<Float> position = getPosition2D(getSkeletonJoint(user, type));
		json.append("{");
		json.append("x:").append(position.getX());
		json.append(" y:").append(position.getY());
		json.append("}");
		return json.toString();
	}

	default String getPoint3DJson(UserData user, JointType type) {
		StringBuffer json = new StringBuffer();
		json.append("{");
		json.append("x:").append(getPositionX(user, type));
		json.append(" y:").append(getPositionY(user, type));
		json.append(" z:").append(getPositionZ(user, type));
		json.append("}");
		return json.toString();
	}
}
