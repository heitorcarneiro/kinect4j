package br.com.kinect4j.engine.core.listener;

public interface GestureObservable extends Tracking {

	<T extends Enum<T>> void notifyGetureName(T name);

	void reset();
}
