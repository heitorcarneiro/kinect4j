package br.com.kinect4j.engine.core.listener;

import com.primesense.nite.UserData;

public interface Tracking {
	void updateUserTracking(UserData user);
	int getProgressPercentage();
}
