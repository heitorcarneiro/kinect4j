package br.com.kinect4j.engine.core.listener;

public interface PoseObservable extends Tracking {

	<T extends Enum<T>> void notifyPoseName(T name);

	<T extends Enum<T>> T getPoseName();
}
