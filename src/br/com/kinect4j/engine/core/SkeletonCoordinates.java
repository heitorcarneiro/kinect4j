package br.com.kinect4j.engine.core;

import com.primesense.nite.Point3D;
import com.primesense.nite.UserData;

public interface SkeletonCoordinates {
	void addLeftHandPoint(UserData user);

	void addRightHandPoint(UserData user);

	Point3D<Float> getHeadPos(UserData user);

	Point3D<Float> getLeftShoulder(UserData user);

	Point3D<Float> getRightShoulder(UserData user);

	Point3D<Float> getLeftHandCoords(UserData user);

	Point3D<Float> getLeftHandPreviousCoords(UserData user);

	Point3D<Float> getRightHandCoords(UserData user);

	Point3D<Float> getRightHandPreviousCoords(UserData user);

	double getCurrentHandsDistance(UserData user);

	double getPreviousHandsDistance(UserData user);

	double getDistanceLeftHandtoShoulder(UserData user);

	double getDistanceRightHandtoShoulder(UserData user);

	double getRightHandDeltaX(UserData user);

	double getRightHandDeltaY(UserData user);

	double getLeftHandDeltaX(UserData user);

	double getLeftHandDeltaY(UserData user);

}
