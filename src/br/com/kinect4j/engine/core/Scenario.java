package br.com.kinect4j.engine.core;

import java.awt.image.BufferedImage;

public interface Scenario {
	void drawScenario(java.awt.Graphics g, int[] depthPixels, int width, int height, java.awt.Component component);

	int getFramePositionX();

	int getFramePositionY();

	BufferedImage getScenario();

}
