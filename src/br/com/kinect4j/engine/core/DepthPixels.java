package br.com.kinect4j.engine.core;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.openni.VideoFrameRef;

import com.primesense.nite.UserTrackerFrameRef;

/**
 * 
 * @author Heitor
 * @since 2016
 *
 *        calculateHistogram() converts the depth measurements (millimeter
 *        distances from the Kinect) into integers in the range 0-255, which are
 *        stored in histogram[].
 * 
 *        The histogram[] array is used in a rather complex way in
 *        updateDepthImage(). Its easiest to think of its integer values as
 *        8-bit grayscales, while its indices are depth measurements. For
 *        instance, if histogram[100] == 255, then it means that the 100mm depth
 *        is represented by the color white.
 * 
 *        In calculateDepthPixels(), histogram[] and the depth buffer are used
 *        to initialize the imgbytes[] array. The pixel position of each depth
 *        measurement is read from the buffer, and that depth's grayscale from
 *        histogram[]. The grayscale is stored (as a byte) at that pixel
 *        position in imgbytes[].
 * 
 *        At render time (which I describe in the next subsection), the bytes in
 *        imgbytes[] are used to fill the 2D data buffer in the grayscale depth
 *        BufferedImage. calcHistogram() converts depths measurements to
 *        grayscales (integers in the range 0 to 255) in four stages.
 */
public class DepthPixels {
	/**
	 * The histogram[] array is an important non-OpenNI data structure, which is
	 * used to record depth values and to convert them into integers between 0
	 * and 255. These integers are later treated at 8-bit grayscale values, and
	 * used to initialize the grayscale BufferedImage for the depth picture.
	 */
	private float[] histogram;
	private int[] depthPixels;
	private VideoFrameRef depthFrame;

	public DepthPixels(VideoFrameRef depthFrame) {
		this.depthFrame = depthFrame;
	}

	public int[] calculateDepthPixels(UserTrackerFrameRef userLastFrame, Draw drawSkeleton) {

		ByteBuffer frameData = depthFrame.getData().order(ByteOrder.LITTLE_ENDIAN);
		ByteBuffer usersFrame = userLastFrame.getUserMap().getPixels().order(ByteOrder.LITTLE_ENDIAN);

		// make sure we have enough room
		if (depthPixels == null || depthPixels.length < depthFrame.getWidth() * depthFrame.getHeight()) {
			depthPixels = new int[depthFrame.getWidth() * depthFrame.getHeight()];
		}
		calculateHistogram(frameData);

		frameData.rewind();

		int pos = 0;
		while (frameData.remaining() > 0) {
			short depth = frameData.getShort();
			short userId = usersFrame.getShort();
			short pixel = (short) histogram[depth];

			int color = 0xFFFFFFFF;
			if (userId > 0) {
				color = drawSkeleton.getColor(userId);
			}

			depthPixels[pos] = color & (0xFF000000 | (pixel << 16) | (pixel << 8) | pixel);
			pos++;
		}
		return depthPixels;
	}

	/**
	 * calculateHistogram(ByteBuffer depthBuffer): void
	 * 
	 * 1. The first stage resets the histogram[] array.
	 * 
	 * 2. In the second stage, the number of pixels at each distance (or depth)
	 * from the camera is stored in histogram[]. Each depth (an integer
	 * millimeter value) is used as an index into the array. For example,
	 * histogram[100] will record the number of pixels that are 100mm from the
	 * Kinect.
	 * 
	 * 3. The third stage accumulates the histogram[] counts, so that each
	 * histogram[] cell contains the number of the pixels at a certain depth or
	 * less from the Kinect. For instance, histogram[100] will record the number
	 * of pixels that are 100mm or less from the camera. Because the totals are
	 * cummulative, histogram[maxDepth] will hold the total number of pixels in
	 * the depth map.
	 * 
	 * 4. The last stage transforms the cummulative counts into integer values
	 * between 0 and 255. Each histogram[] value is transformed into a fraction
	 * by dividing its value (the number of pixels equal or less than a certain
	 * depth) by the total number of pixels. This will mean that
	 * histogram[maxDepth] will be mapped to 1.0. Each fraction is subtracted
	 * from 1.0, so that larger numbers will be mapped to a fraction closer to
	 * 0. Finally, each fraction is transformed into an integer between 0 and
	 * 255. These integers will later be treated as 8-bit grayscales, and so
	 * greater depths will have smaller grayscale values, and so be rendered
	 * nearer to black.
	 * 
	 * calculateHistogram() treats histogram[0] as a special case. It's used to
	 * represent "no value", which occurs when a pixel is too close to the
	 * Kinect to be assigned a depth. Since histogram[0] always stores 0, it
	 * will be mapped to black when imgbytes[] is converted into grayscale image
	 * data.
	 */
	public void calculateHistogram(ByteBuffer depthBuffer) {
		// make sure we have enough room
		if (histogram == null) {
			histogram = new float[10000];
		}

		// reset
		for (int i = 0; i < histogram.length; ++i)
			histogram[i] = 0;

		int points = 0;
		while (depthBuffer.remaining() > 0) {
			int depth = depthBuffer.getShort() & 0xFFFF;
			if (depth != 0) {
				histogram[depth]++;
				points++;
			}
		}

		for (int i = 1; i < histogram.length; i++) {
			histogram[i] += histogram[i - 1];
		}

		if (points > 0) {
			for (int i = 1; i < histogram.length; i++) {
				histogram[i] = (int) (256 * (1.0f - (histogram[i] / (float) points)));
			}
		}
	}
}
