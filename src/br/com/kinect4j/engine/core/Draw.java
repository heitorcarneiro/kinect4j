package br.com.kinect4j.engine.core;

import java.awt.Color;
import java.awt.Graphics;

import com.primesense.nite.UserData;

public interface Draw {
	void drawSkeleton(UserData user, Graphics g, int framePosX, int framePosY);

	Color getColor(UserData user);

	int getColor(short userId);

}
