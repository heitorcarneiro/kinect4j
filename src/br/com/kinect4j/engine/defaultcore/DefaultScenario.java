package br.com.kinect4j.engine.defaultcore;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import br.com.kinect4j.engine.core.Scenario;

public class DefaultScenario implements Scenario {
	private BufferedImage scenario;
	private int framePositionX = 0;
	private int framePositionY = 0;

	@Override
	public void drawScenario(Graphics g, int[] depthPixels, int width, int height, Component component) {
		if (this.scenario == null || this.scenario.getWidth() != width || this.scenario.getHeight() != height) {
			this.scenario = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		}
		scenario.setRGB(0, 0, width, height, depthPixels, 0, width);

		framePositionX = (component.getWidth() - width) / 2;
		framePositionY = (component.getHeight() - height) / 2;

		g.drawImage(this.scenario, framePositionX, framePositionY, null);
	}

	@Override
	public int getFramePositionX() {
		return framePositionX;
	}

	@Override
	public int getFramePositionY() {
		return framePositionY;
	}

	@Override
	public BufferedImage getScenario() {
		return scenario;
	}

}
