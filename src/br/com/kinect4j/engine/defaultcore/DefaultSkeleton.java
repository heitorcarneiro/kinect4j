package br.com.kinect4j.engine.defaultcore;

import java.util.HashMap;

import com.primesense.nite.JointType;
import com.primesense.nite.Point2D;
import com.primesense.nite.Point3D;
import com.primesense.nite.SkeletonJoint;
import com.primesense.nite.UserData;
import com.primesense.nite.UserTracker;

import br.com.kinect4j.engine.core.JointRelation;
import br.com.kinect4j.engine.core.Skeleton;

public class DefaultSkeleton implements Skeleton {
	private UserTracker userTracker;

	public DefaultSkeleton(UserTracker userTracker) {
		this.userTracker = userTracker;
	}

	@Override
	public SkeletonJoint getSkeletonJoint(UserData user, JointType type) {
		return user.getSkeleton().getJoint(type);
	}

	@Override
	public Point2D<Float> getPosition2D(SkeletonJoint skeletonJoint) {
		return this.userTracker.convertJointCoordinatesToDepth(skeletonJoint.getPosition());
	}

	@Override
	public Point3D<Float> getPosition3D(SkeletonJoint skeletonJoint) {
		return skeletonJoint.getPosition();
	}

	@Override
	public Point3D<Float> getPosition3D(UserData user, JointType type) {
		return getPosition3D(getSkeletonJoint(user, type));
	}

	@Override
	public Point3D<Float> getPosition3D(HashMap<JointType, SkeletonJoint> skel, JointType type) {
		SkeletonJoint position = skel.get(type);

		if (position == null) {
			return null;
		}

		if (position.getPositionConfidence() == 0) {
			return null;
		}

		return position.getPosition();
	}

	@Override
	public float getPositionX(UserData user, JointType type) {
		return user.getSkeleton().getJoint(type).getPosition().getX();
	}

	@Override
	public float getPositionY(UserData user, JointType type) {
		return user.getSkeleton().getJoint(type).getPosition().getY();
	}

	@Override
	public float getPositionZ(UserData user, JointType type) {
		return user.getSkeleton().getJoint(type).getPosition().getZ();
	}

	@Override
	public boolean checkSkeletonPosition(UserData user, JointType from, JointRelation relation, JointType to) {
		Point3D<Float> fromPosition = getPosition3D(getSkeletonJoint(user, from));
		Point3D<Float> toPosition = getPosition3D(getSkeletonJoint(user, to));

		boolean result = false;

		switch (relation) {
		case ABOVE:
			result = (fromPosition.getY() > toPosition.getY());
			break;
		case BELOW:
			result = (fromPosition.getY() < toPosition.getY());
			break;
		case LEFT_OF:
			result = (fromPosition.getX() < toPosition.getX());
			break;
		case RIGHT_OF:
			result = (fromPosition.getX() > toPosition.getX());
			break;
		case CLOSER_TO:
			result = (fromPosition.getZ() < toPosition.getZ());
			break;
		case FAR_AWAY:
			result = (fromPosition.getZ() > toPosition.getZ());
			break;
		case EQUALS_VERTICAL:
			result = (compareWithMarginError(getMarginError(), fromPosition.getX(), toPosition.getX()));
			break;
		case EQUALS_HORIZONTAL:
			result = (compareWithMarginError(getMarginError(), fromPosition.getY(), toPosition.getY()));
			break;
		}

		return result;
	}

	@Override
	public boolean comparePositionX(UserData user, JointType from, Operator relation, JointType to) {
		float positionFrom = getPositionX(user, from);
		float positionTo = getPositionX(user, to);

		return comparePosition(positionFrom, relation, positionTo);
	}

	@Override
	public boolean comparePositionY(UserData user, JointType from, Operator relation, JointType to) {
		float positionFrom = getPositionY(user, from);
		float positionTo = getPositionY(user, to);

		return comparePosition(positionFrom, relation, positionTo);
	}

	@Override
	public boolean comparePositionZ(UserData user, JointType from, Operator relation, JointType to) {
		float positionFrom = getPositionZ(user, from);
		float positionTo = getPositionZ(user, to);

		return comparePosition(positionFrom, relation, positionTo);
	}

	@Override
	public double getMarginError() {
		return 10;
	}

}
