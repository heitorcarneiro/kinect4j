package br.com.kinect4j.engine.defaultcore;

import java.awt.Color;
import java.awt.Graphics;

import com.primesense.nite.JointType;
import com.primesense.nite.Point2D;
import com.primesense.nite.SkeletonJoint;
import com.primesense.nite.UserData;

import br.com.kinect4j.engine.core.Draw;
import br.com.kinect4j.engine.core.Skeleton;

public class DefaultDraw implements Draw {
	private int[] skeletonColors;
	private Skeleton skeleton;

	public DefaultDraw(Skeleton skeleton) {
		skeletonColors = new int[] { 0xFFFF0000, 0xFF00FF00, 0xFF0000FF, 0xFFFFFF00, 0xFFFF00FF, 0xFF00FFFF };
		this.skeleton = skeleton;
	}

	@Override
	public void drawSkeleton(UserData user, Graphics g, int framePosX, int framePosY) {
		drawLimb(g, framePosX, framePosY, user, JointType.HEAD, JointType.NECK);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_SHOULDER, JointType.LEFT_ELBOW);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_ELBOW, JointType.LEFT_HAND);
		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_SHOULDER, JointType.RIGHT_ELBOW);
		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_ELBOW, JointType.RIGHT_HAND);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_SHOULDER, JointType.RIGHT_SHOULDER);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_SHOULDER, JointType.TORSO);
		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_SHOULDER, JointType.TORSO);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_HIP, JointType.TORSO);
		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_HIP, JointType.TORSO);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_HIP, JointType.RIGHT_HIP);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_HIP, JointType.LEFT_KNEE);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_KNEE, JointType.LEFT_FOOT);
		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_HIP, JointType.RIGHT_KNEE);
		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_KNEE, JointType.RIGHT_FOOT);
	}

	@Override
	public Color getColor(UserData user) {
		return new Color(this.skeletonColors[(user.getId() + 1) % this.skeletonColors.length]);
	}

	@Override
	public int getColor(short userId) {
		return skeletonColors[userId % skeletonColors.length];
	}

	private void drawLine(Graphics g, int framePosX, int framePosY, Point2D<Float> fromPosition,
			Point2D<Float> toPosition) {
		int x1 = framePosX + fromPosition.getX().intValue();
		int y1 = framePosY + fromPosition.getY().intValue();
		int x2 = framePosX + toPosition.getX().intValue();
		int y2 = framePosY + toPosition.getY().intValue();
		g.drawLine(x1, y1, x2, y2);
	}

	private void drawLimb(Graphics g, int framePosX, int framePosY, UserData user, JointType from, JointType to) {
		SkeletonJoint fromJoint = this.skeleton.getSkeletonJoint(user, from);
		SkeletonJoint toJoint = this.skeleton.getSkeletonJoint(user, to);
		if (validPositionConfidence(fromJoint, toJoint)) {
			return;
		}
		Point2D<Float> fromPosition = this.skeleton.getPosition2D(fromJoint);
		Point2D<Float> toPosition = this.skeleton.getPosition2D(toJoint);
		// draw it in another color than the use color
		g.setColor(getColor(user));
		drawLine(g, framePosX, framePosY, fromPosition, toPosition);
	}

	private boolean validPositionConfidence(SkeletonJoint fromJoint, SkeletonJoint toJoint) {
		return (fromJoint.getPositionConfidence() == 0.0 || toJoint.getPositionConfidence() == 0.0);
	}

}
