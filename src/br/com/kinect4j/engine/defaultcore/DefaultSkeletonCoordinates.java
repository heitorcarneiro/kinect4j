package br.com.kinect4j.engine.defaultcore;

import java.util.ArrayList;
import java.util.List;

import com.primesense.nite.JointType;
import com.primesense.nite.Point3D;
import com.primesense.nite.UserData;

import br.com.kinect4j.engine.core.Skeleton;
import br.com.kinect4j.engine.core.SkeletonCoordinates;

public class DefaultSkeletonCoordinates implements SkeletonCoordinates {
	private static final int MAX_POINTS = 4;
	private final Skeleton skeleton;
	private List<Point3D<Float>> leftHandCoords;
	private List<Point3D<Float>> rightHandCoords;

	public DefaultSkeletonCoordinates(Skeleton skeleton) {
		this.skeleton = skeleton;
		this.leftHandCoords = new ArrayList<>(MAX_POINTS);
		this.rightHandCoords = new ArrayList<>(MAX_POINTS);
	}

	@Override
	public synchronized void addLeftHandPoint(UserData user) {
		Point3D<Float> realPt = skeleton.getPosition3D(user, JointType.LEFT_HAND);
		leftHandCoords.add(realPt);
		if (leftHandCoords.size() > MAX_POINTS) // get rid of the oldest
			leftHandCoords.remove(0);

	}

	@Override
	public synchronized void addRightHandPoint(UserData user) {
		Point3D<Float> realPt = skeleton.getPosition3D(user, JointType.RIGHT_HAND);
		rightHandCoords.add(realPt);
		if (rightHandCoords.size() > MAX_POINTS) // get rid of the oldest point
			rightHandCoords.remove(0);

	}

	@Override
	public Point3D<Float> getHeadPos(UserData user) {
		return skeleton.getPosition3D(user, JointType.HEAD);
	}

	@Override
	public Point3D<Float> getLeftShoulder(UserData user) {
		return skeleton.getPosition3D(user, JointType.LEFT_SHOULDER);
	}

	@Override
	public Point3D<Float> getRightShoulder(UserData user) {
		return skeleton.getPosition3D(user, JointType.RIGHT_SHOULDER);
	}

	@Override
	public Point3D<Float> getLeftHandCoords(UserData user) {
		if (leftHandCoords.size() >= MAX_POINTS) {
			return leftHandCoords.get(MAX_POINTS - 1);
		} else {
			return leftHandCoords.get(0);
		}
	}

	@Override
	public Point3D<Float> getRightHandCoords(UserData user) {
		if (rightHandCoords.size() == MAX_POINTS) {
			return rightHandCoords.get(MAX_POINTS - 1);
		} else {
			return rightHandCoords.get(0);
		}
	}

	/**
	 * Previous coords
	 * 
	 * @param user
	 * @return
	 */
	@Override
	public Point3D<Float> getLeftHandPreviousCoords(UserData user) {
		return leftHandCoords.get(0);
	}

	@Override
	public Point3D<Float> getRightHandPreviousCoords(UserData user) {
		return rightHandCoords.get(0);
	}

	/**
	 * Util methods
	 */

	@Override
	public double getCurrentHandsDistance(UserData user) {
		return skeleton.calculateEuclideanDistance3D(getLeftHandCoords(user), getRightHandCoords(user));
	}

	@Override
	public double getPreviousHandsDistance(UserData user) {
		return skeleton.calculateEuclideanDistance3D(getLeftHandPreviousCoords(user), getRightHandPreviousCoords(user));

	}

	@Override
	public double getDistanceLeftHandtoShoulder(UserData user) {
		return skeleton.calculateEuclideanDistance3D(getLeftShoulder(user), getLeftHandCoords(user));

	}

	@Override
	public double getDistanceRightHandtoShoulder(UserData user) {
		return skeleton.calculateEuclideanDistance3D(getRightShoulder(user), getRightHandCoords(user));

	}

	@Override
	public double getRightHandDeltaX(UserData user) {
		return getRightHandPreviousCoords(user).getX() - getRightHandCoords(user).getX();
	}

	@Override
	public double getRightHandDeltaY(UserData user) {
		return getRightHandPreviousCoords(user).getY() - getRightHandCoords(user).getY();
	}

	@Override
	public double getLeftHandDeltaX(UserData user) {
		return getLeftHandPreviousCoords(user).getX() - getLeftHandCoords(user).getX();

	}

	@Override
	public double getLeftHandDeltaY(UserData user) {
		return getLeftHandPreviousCoords(user).getY() - getLeftHandCoords(user).getY();
	}

}
