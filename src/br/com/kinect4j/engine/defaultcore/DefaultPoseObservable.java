package br.com.kinect4j.engine.defaultcore;

import com.primesense.nite.UserData;

import br.com.kinect4j.engine.core.GestureResult;
import br.com.kinect4j.engine.core.Pose;
import br.com.kinect4j.engine.core.listener.PoseObservable;
import br.com.kinect4j.view.Kinect4jObserver;

public class DefaultPoseObservable<T extends Enum<T>> implements PoseObservable {
	/**
	 * The current frame that we are on
	 */
	private int frameCount = 0;
	/**
	 * The numbers of frame to identify the pose
	 */
	private int poseNumberFrames = 0;
	/**
	 * The type of gesture that this is
	 */

	private T type;
	/**
	 * The parts that make up this gesture
	 */
	private Pose pose;
	/**
	 * Used to update the gesture type
	 */
	private Kinect4jObserver subject;

	/**
	 * 
	 * @param pose
	 * @param type
	 * @param subject
	 */
	public DefaultPoseObservable(int poseNumberFrames, Pose pose, T type, Kinect4jObserver subject) {
		this.poseNumberFrames = poseNumberFrames;
		this.pose = pose;
		this.type = type;
		this.subject = subject;
	}

	@Override
	public void updateUserTracking(UserData user) {

		if (pose.checkPose(user) == GestureResult.SUCCEED)
		// if is valid, start frame count
		{
			if (getPoseNumberFrames() == getFrameCount())
			// complete frame count, notify and reset
			{
				notifyPoseName(type);
				resetFrameCount();
			} else
			// wait
			{
				incrementFrameCount();
			}
		} else
		// reset frame count
		{
			resetFrameCount();
		}
	}

	private void incrementFrameCount() {
		this.frameCount = getFrameCount() + 1;
	}

	private void resetFrameCount() {
		this.frameCount = 0;
	}

	private int getFrameCount() {
		return frameCount;
	}

	private int getPoseNumberFrames() {
		return poseNumberFrames;
	}

	@SuppressWarnings("hiding")
	@Override
	public <T extends Enum<T>> void notifyPoseName(T name) {
		subject.updatePoseName(name);

	}

	@Override
	public int getProgressPercentage() {
		return getFrameCount() * 100 / getPoseNumberFrames();
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getPoseName() {
		return type;
	}

}
