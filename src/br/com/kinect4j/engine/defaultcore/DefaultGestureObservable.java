package br.com.kinect4j.engine.defaultcore;

import java.util.List;

import com.primesense.nite.UserData;

import br.com.kinect4j.engine.core.GestureResult;
import br.com.kinect4j.engine.core.KeyFrame;
import br.com.kinect4j.engine.core.listener.GestureObservable;
import br.com.kinect4j.view.Kinect4jObserver;

public class DefaultGestureObservable<T extends Enum<T>> implements GestureObservable {

	/**
	 * The current frame that we are on
	 */
	private int frameCount = 0;
	private List<KeyFrame> frames;
	private KeyFrame actual;
	/**
	 * The current gesture part that we are matching against
	 */
	private int currentKeyFrame = 0;
	/**
	 * The type of gesture that this is
	 */

	private T type;
	/**
	 * Used to update the gesture type
	 */
	private Kinect4jObserver subject;

	/**
	 * 
	 * @param frames
	 * @param subject
	 */
	public DefaultGestureObservable(List<KeyFrame> frames, T type, Kinect4jObserver subject) {
		this.frames = frames;
		this.type = type;
		this.subject = subject;
		this.actual = getFirst();
	}

	private void nextKeyFrame() {
		currentKeyFrame++;
		resetFrameCount();
		actual = frames.get(currentKeyFrame);
	}

	@Override
	public void reset() {
		resetFrameCount();
		currentKeyFrame = 0;
		actual = frames.get(currentKeyFrame);
	}

	private void continueTracking() {
		incrementFrameCount();
	}

	@Override
	public void updateUserTracking(UserData user) {
		// current key frame
		actual = frames.get(currentKeyFrame);
		int inferior = actual.getInferiorLimitFrame();
		int superior = actual.getSuperiorLimitFrame();

		GestureResult result = actual.getPose().checkPose(user);

		if (result == GestureResult.SUCCEED)
		// if is valid, start frame count
		{
			if (actual.equals(getLast()))
			// check if is the last element, gesture is recognized!
			{
				notifyGetureName(type);
				reset();
			} else
			// continue tracking..
			{
				if (getFrameCount() >= inferior && getFrameCount() <= superior)
				// check if still is valid, inside of limits!
				{
					nextKeyFrame();
				} else if (getFrameCount() < inferior)
				// still waiting the user do that key frame
				{
					continueTracking();
				}

				else if (getFrameCount() > superior)
				// limit of keys frames is passed, reset
				{
					reset();
				}
			}

		} else
		// check if is pausing?
		{
			if (superior < getFrameCount())
			// reset
			{
				reset();
			} else
			// movement is pausing, continue tracking
			{
				continueTracking();
			}
		}

	}

	@Override
	public int getProgressPercentage() {
		return currentKeyFrame * 100 / getSize();
	}

	@SuppressWarnings("hiding")
	@Override
	public <T extends Enum<T>> void notifyGetureName(T name) {
		subject.updateGestureName(name);

	}

	private void incrementFrameCount() {
		this.frameCount = getFrameCount() + 1;
	}

	private void resetFrameCount() {
		this.frameCount = 0;
	}

	private int getFrameCount() {
		return frameCount;
	}

	private int getSize() {
		return this.frames.size();
	}

	private KeyFrame getFirst() {
		return this.frames.get(0);
	}

	private KeyFrame getLast() {
		return this.frames.get(getSize() - 1);
	}

}
