package br.com.kinect4j.controller;

@FunctionalInterface
public interface Controller {
	void kinectActionPerformed();
}
