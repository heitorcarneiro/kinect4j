package br.com.kinect4j.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.primesense.nite.UserData;

import br.com.kinect4j.controller.movements.RightHandSwipeLeftToRight;
import br.com.kinect4j.controller.movements.RightHandSwipeRightToLeft;
import br.com.kinect4j.controller.movements.WaveLeft;
import br.com.kinect4j.controller.movements.WaveRight;
import br.com.kinect4j.controller.movements.ZoomIn;
import br.com.kinect4j.controller.movements.ZoomOut;
import br.com.kinect4j.engine.core.GestureController;
import br.com.kinect4j.engine.core.KeyFrame;
import br.com.kinect4j.engine.core.Pose;
import br.com.kinect4j.engine.core.Skeleton;
import br.com.kinect4j.engine.core.listener.GestureObservable;
import br.com.kinect4j.engine.defaultcore.DefaultGestureObservable;
import br.com.kinect4j.view.Kinect4jObserver;

public class DefaultGestureController<T extends Enum<T>> implements GestureController {
	private Map<T, List<KeyFrame>> frames;
	private List<GestureObservable> gestures;

	public DefaultGestureController(Skeleton skeleton, Kinect4jObserver subject) {
		frames = keyFrameFactory(skeleton);
		gestures = new ArrayList<GestureObservable>();
		/**
		 * add mock
		 */
		addDefaultGesture(subject);
	}
	
	@SuppressWarnings("hiding")
	@Override
	public <T extends Enum<T>> void addGesture(Kinect4jObserver subject, T type, Pose... gestureParts) {
		int size = gestureParts.length;

		if (size == 0)
			return;

		KeyFrame[] frames = new KeyFrame[size];

		for (int i = 0; i < size; i++) {

			KeyFrame k;

			if (i == 0) {
				k = new KeyFrame(gestureParts[i], 0, 0);
			} else {
				k = new KeyFrame(gestureParts[i], 1, 25);
			}

			frames[i] = k;
		}

		addGesture(subject, type, frames);
		
	}
	@SuppressWarnings({ "unchecked", "rawtypes", "hiding" })
	@Override
	public <T extends Enum<T>> void addGesture(Kinect4jObserver subject, T name, KeyFrame... frames) {
		List<KeyFrame> keys = new ArrayList<>();
		Collections.addAll(keys, frames);
		GestureObservable gesture = new DefaultGestureObservable(keys, name, subject);
		gestures.add(gesture);
		
	}

	@Override
	public void updateAllGestures(UserData user) {
		for (GestureObservable g : gestures) {
			g.updateUserTracking(user);
		}

	}

	@Override
	public boolean removeGesture(GestureObservable gesture) {
		return gestures.remove(gesture);
	}

	@Override
	public void resetGesture(GestureObservable gesture) {
		gesture.reset();
	}

	@Override
	public void resetAllGestures() {
		for (GestureObservable g : this.gestures) {
			g.reset();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void addDefaultGesture(Kinect4jObserver subject) {
		for (Entry<T, List<KeyFrame>> k : frames.entrySet()) {
			GestureObservable gesture = new DefaultGestureObservable(k.getValue(), k.getKey(), subject);
			gestures.add(gesture);
		}
	}

	@SuppressWarnings("unchecked")
	private Map<T, List<KeyFrame>> keyFrameFactory(Skeleton skeleton) {

		// WaveLeft

		WaveLeft waveLeft = new WaveLeft(skeleton);
		KeyFrame lk1 = new KeyFrame(waveLeft.getSegments()[0], 0, 0); // inicial
		KeyFrame lk2 = new KeyFrame(waveLeft.getSegments()[1], 1, 25);
		List<KeyFrame> waveLeftFrames = new ArrayList<>();
		waveLeftFrames.add(lk1);
		waveLeftFrames.add(lk2);

		// WaveRight

		WaveRight waveRight = new WaveRight(skeleton);
		KeyFrame k1 = new KeyFrame(waveRight.getSegments()[0], 0, 0); // inicial
		KeyFrame k2 = new KeyFrame(waveRight.getSegments()[1], 1, 25);
		List<KeyFrame> waveRightFrames = new ArrayList<>();
		waveRightFrames.add(k1);
		waveRightFrames.add(k2);

		// SwipeRightToLeft

		RightHandSwipeRightToLeft swipeLeft = new RightHandSwipeRightToLeft(skeleton);
		KeyFrame sLK1 = new KeyFrame(swipeLeft.getSegments()[0], 0, 0);
		KeyFrame sLK2 = new KeyFrame(swipeLeft.getSegments()[1], 1, 25);
		KeyFrame sLK3 = new KeyFrame(swipeLeft.getSegments()[2], 1, 25);
		List<KeyFrame> swipeLeftFrames = Arrays.asList(sLK1, sLK2, sLK3);

		// SwipeLeftToRight

		RightHandSwipeLeftToRight swipeRight = new RightHandSwipeLeftToRight(skeleton);
		KeyFrame sRK1 = new KeyFrame(swipeRight.getSegments()[0], 0, 0);
		KeyFrame sRK2 = new KeyFrame(swipeRight.getSegments()[1], 1, 25);
		KeyFrame sRK3 = new KeyFrame(swipeRight.getSegments()[2], 1, 25);
		List<KeyFrame> swipeRightFrames = Arrays.asList(sRK1, sRK2, sRK3);

		// Zoom in
		ZoomIn zoomIn = new ZoomIn(skeleton);
		KeyFrame sZI1 = new KeyFrame(zoomIn.getSegments()[0], 0, 0);
		KeyFrame sZI2 = new KeyFrame(zoomIn.getSegments()[1], 1, 25);
		KeyFrame sZI3 = new KeyFrame(zoomIn.getSegments()[2], 1, 25);
		List<KeyFrame> zoomInFrames = Arrays.asList(sZI1, sZI2, sZI3);

		// Zoom out
		ZoomOut zoomOut = new ZoomOut(skeleton);
		KeyFrame sZO1 = new KeyFrame(zoomOut.getSegments()[0], 0, 0);
		KeyFrame sZO2 = new KeyFrame(zoomOut.getSegments()[1], 1, 25);
		KeyFrame sZO3 = new KeyFrame(zoomOut.getSegments()[2], 1, 25);
		List<KeyFrame> zoomOutFrames = Arrays.asList(sZO1, sZO2, sZO3);
		/**
		 * Add in map
		 */
		Map<DefaultGestureName, List<KeyFrame>> result = new HashMap<>();

		result.put(DefaultGestureName.WAVE_LEFT, waveLeftFrames);
		result.put(DefaultGestureName.WAVE_RIGHT, waveRightFrames);
		result.put(DefaultGestureName.SWIPE_RIGHT_TO_LEFT, swipeLeftFrames);
		result.put(DefaultGestureName.SWIPE_LEFT_TO_RIGHT, swipeRightFrames);
		result.put(DefaultGestureName.ZOOM_IN, zoomInFrames);
		result.put(DefaultGestureName.ZOOM_OUT, zoomOutFrames);

		return (Map<T, List<KeyFrame>>) result;
	}

	

}
