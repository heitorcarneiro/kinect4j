package br.com.kinect4j.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.primesense.nite.UserData;

import br.com.kinect4j.engine.core.Pose;
import br.com.kinect4j.engine.core.PoseController;
import br.com.kinect4j.engine.core.listener.PoseObservable;
import br.com.kinect4j.engine.defaultcore.DefaultPoseObservable;
import br.com.kinect4j.view.Kinect4jObserver;

public class DefaultPoseController<T extends Enum<T>> implements PoseController {
	private List<PoseObservable> poses;
	private Map<T, Integer> percentage;

	public DefaultPoseController() {
		this.poses = new ArrayList<>();
		this.percentage = new HashMap<>();
	}

	@Override
	public void updateAllPoses(UserData user) {
		for (PoseObservable p : poses) {
			p.updateUserTracking(user);
			percentage.put(p.getPoseName(), p.getProgressPercentage());
		}
	}

	@SuppressWarnings({ "rawtypes", "hiding", "unchecked" })
	@Override
	public <T extends Enum<T>> void addPose(T name, int poseNumberFrames, Pose pose, Kinect4jObserver subject) {
		PoseObservable p = new DefaultPoseObservable(poseNumberFrames, pose, name, subject);
		poses.add(p);
	}

	@SuppressWarnings("hiding")
	@Override
	public <T extends Enum<T>> Integer getProgressPercentage(T name) {
		return percentage.get(name);
	}

	@Override
	public boolean removePose(PoseObservable pose) {
		return poses.remove(pose);
	}
}
