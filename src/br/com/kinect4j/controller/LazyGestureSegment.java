package br.com.kinect4j.controller;

import br.com.kinect4j.engine.core.GestureResult;
import br.com.kinect4j.engine.core.Pose;
import br.com.kinect4j.engine.core.Skeleton;

public abstract class LazyGestureSegment implements Pose {
	protected Skeleton skeleton;

	protected LazyGestureSegment(Skeleton skeleton) {
		this.skeleton = skeleton;
	}

	protected GestureResult checkCondition(boolean... rules) {
		int size = rules.length;
		if (size > 1)
		/*
		 * Caso rules tenha mais de um elemento inicia a verificao. Se o
		 * primeiro elemento for falso retorna FAIL, senao continua a execucao.
		 */
		{
			if (!rules[0]) {
				return GestureResult.FAIL;
			}
			/**
			 * Se todos os elementos restantes forem true, retorna SUCCEED,
			 * senao returna PAUSING.
			 */
			boolean result = true;
			
			for(Boolean rule: rules){
				result = (result && rule);
			}

			if (result == false) {
				return GestureResult.PAUSING;
			} else {
				return GestureResult.SUCCEED;
			}

		} else if (size == 1)
		/*
		 * Caso seja uma codicao, apenas valida a pose.
		 */
		{
			if (!rules[0]) {
				return GestureResult.FAIL;
			} else {
				return GestureResult.SUCCEED;
			}
		}
		/**
		 * Default retorna FAIL.
		 */
		return GestureResult.FAIL;
	}

}
