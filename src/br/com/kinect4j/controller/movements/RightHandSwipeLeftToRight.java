package br.com.kinect4j.controller.movements;

import com.primesense.nite.JointType;
import com.primesense.nite.UserData;

import br.com.kinect4j.controller.LazyGestureSegment;
import br.com.kinect4j.engine.core.GestureResult;
import br.com.kinect4j.engine.core.Pose;
import br.com.kinect4j.engine.core.Skeleton;
import br.com.kinect4j.engine.core.Skeleton.Operator;

public class RightHandSwipeLeftToRight {
	private Pose segment1;
	private Pose segment2;
	private Pose segmento3;
	private Pose[] segments;

	public RightHandSwipeLeftToRight(Skeleton skeleton) {
		segment1 = new SwipeRightSegment1(skeleton);
		segment2 = new SwipeRightSegment2(skeleton);
		segmento3 = new SwipeRightSegment3(skeleton);
		segments = new Pose[] { segment1, segment2, segmento3 };
	}

	public Pose[] getSegments() {
		return segments;
	}

}

// Segment 1

class SwipeRightSegment1 extends LazyGestureSegment {

	protected SwipeRightSegment1(Skeleton skeleton) {
		super(skeleton);
	}

	@Override
	public GestureResult checkPose(UserData user) {
		// right hand in front of right shoulder
		boolean condition1 = (skeleton.comparePositionZ(user, JointType.RIGHT_HAND, Operator.LESS_THAN,
				JointType.RIGHT_ELBOW))
				&& skeleton.comparePositionY(user, JointType.LEFT_HAND, Operator.LESS_THAN, JointType.NECK);

		// right hand below shoulder height but above hip height
		boolean condition2 = (skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.LESS_THAN, JointType.HEAD))
				&& skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.GREATER_THAN, JointType.TORSO);

		// right hand left of center hip
		boolean condition3 = (skeleton.comparePositionX(user, JointType.RIGHT_HAND, Operator.LESS_THAN,
				JointType.RIGHT_SHOULDER));

		return super.checkCondition(condition1, condition2, condition3);
	}

}

// Segment 2

class SwipeRightSegment2 extends LazyGestureSegment {

	protected SwipeRightSegment2(Skeleton skeleton) {
		super(skeleton);
	}

	@Override
	public GestureResult checkPose(UserData user) {
		// right hand in front of right shoulder
		boolean condition1 = (skeleton.comparePositionZ(user, JointType.RIGHT_HAND, Operator.LESS_THAN,
				JointType.RIGHT_ELBOW))
				&& skeleton.comparePositionY(user, JointType.LEFT_HAND, Operator.LESS_THAN, JointType.NECK);

		// right hand below shoulder height but above hip height
		boolean condition2 = (skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.LESS_THAN, JointType.HEAD))
				&& skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.GREATER_THAN, JointType.TORSO);

		// right hand left of right shoulder & right of left shoulder
		boolean condition3 = (skeleton.comparePositionX(user, JointType.RIGHT_HAND, Operator.LESS_THAN,
				JointType.RIGHT_SHOULDER)
				&& skeleton.comparePositionX(user, JointType.RIGHT_HAND, Operator.GREATER_THAN,
						JointType.LEFT_SHOULDER));

		return super.checkCondition(condition1, condition2, condition3);
	}

}

// Segment 3

class SwipeRightSegment3 extends LazyGestureSegment {

	protected SwipeRightSegment3(Skeleton skeleton) {
		super(skeleton);
	}

	@Override
	public GestureResult checkPose(UserData user) {
		// right hand in front of right shoulder
		boolean condition1 = (skeleton.comparePositionZ(user, JointType.RIGHT_HAND, Operator.LESS_THAN,
				JointType.RIGHT_ELBOW))
				&& skeleton.comparePositionY(user, JointType.LEFT_HAND, Operator.LESS_THAN, JointType.NECK);

		// right hand below shoulder height but above hip height
		boolean condition2 = (skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.LESS_THAN, JointType.HEAD))
				&& skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.GREATER_THAN, JointType.TORSO);

		// right hand right of right shoulder
		boolean condition3 = (skeleton.comparePositionX(user, JointType.RIGHT_HAND, Operator.GREATER_THAN,
				JointType.RIGHT_SHOULDER));

		return super.checkCondition(condition1, condition2, condition3);
	}

}