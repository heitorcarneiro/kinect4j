package br.com.kinect4j.controller.movements;

import com.primesense.nite.JointType;
import com.primesense.nite.UserData;

import br.com.kinect4j.controller.LazyGestureSegment;
import br.com.kinect4j.engine.core.GestureResult;
import br.com.kinect4j.engine.core.Pose;
import br.com.kinect4j.engine.core.Skeleton;

public class WaveLeft {
	private Pose segment1;
	private Pose segment2;
	private Pose[] segments;
	
	public WaveLeft(Skeleton skeleton) {
		segment1 = new WaveLeftSegment1(skeleton);
		segment2 = new WaveLeftSegment2(skeleton);
		segments = new Pose[] {segment1, segment2};
	}

	public Pose[] getSegments() {
		return segments;
	}
}

class WaveLeftSegment1 extends LazyGestureSegment{

	protected WaveLeftSegment1(Skeleton skeleton) {
		super(skeleton);
	}

	@Override
	public GestureResult checkPose(UserData user) {
		// hand above head
		boolean condition1 = (skeleton.getPositionY(user, JointType.LEFT_HAND) > skeleton.getPositionY(user,
				JointType.HEAD));
		// hand right of elbow
		boolean condition2 = (skeleton.getPositionX(user, JointType.LEFT_HAND) > skeleton.getPositionX(user,
				JointType.LEFT_ELBOW));
		return super.checkCondition(condition1, condition2);
	}
}

class WaveLeftSegment2 extends LazyGestureSegment{

	protected WaveLeftSegment2(Skeleton skeleton) {
		super(skeleton);
	}

	@Override
	public GestureResult checkPose(UserData user) {
		// hand above head
		boolean condition1 = (skeleton.getPositionY(user, JointType.LEFT_HAND) > skeleton.getPositionY(user,
				JointType.HEAD));
		// hand left of elbow
		boolean condition2 = (skeleton.getPositionX(user, JointType.LEFT_HAND) < skeleton.getPositionX(user,
				JointType.LEFT_ELBOW));
		return super.checkCondition(condition1, condition2);
	}

}
