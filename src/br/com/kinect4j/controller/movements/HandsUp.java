package br.com.kinect4j.controller.movements;

import com.primesense.nite.JointType;
import com.primesense.nite.UserData;

import br.com.kinect4j.engine.core.GestureResult;
import br.com.kinect4j.engine.core.JointRelation;
import br.com.kinect4j.engine.core.Pose;
import br.com.kinect4j.engine.core.Skeleton;

public class HandsUp implements Pose {
	private Skeleton skeleton;

	public HandsUp(Skeleton skeleton) {
		this.skeleton = skeleton;
	}

	@Override
	public GestureResult checkPose(UserData user) {
		boolean rightUp = skeleton.checkSkeletonPosition(user, JointType.RIGHT_HAND, JointRelation.ABOVE, JointType.HEAD);
		boolean leftUp = skeleton.checkSkeletonPosition(user, JointType.LEFT_HAND, JointRelation.ABOVE, JointType.HEAD);
		return (rightUp && leftUp)? GestureResult.SUCCEED: GestureResult.FAIL;
	}

}
