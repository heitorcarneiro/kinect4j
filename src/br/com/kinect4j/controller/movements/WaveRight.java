package br.com.kinect4j.controller.movements;

import com.primesense.nite.JointType;
import com.primesense.nite.UserData;

import br.com.kinect4j.engine.core.GestureResult;
import br.com.kinect4j.engine.core.Pose;
import br.com.kinect4j.engine.core.Skeleton;

public class WaveRight {
	private Pose segment1;
	private Pose segment2;
	private Pose[] segments;
	
	public WaveRight(Skeleton skeleton) {
		segment1 = new WaveRightSegment1(skeleton);
		segment2 = new WaveRightSegment2(skeleton);
		segments = new Pose[] {segment1, segment2};
	}

	public Pose[] getSegments() {
		return segments;
	}
}

class WaveRightSegment1 implements Pose {
	private Skeleton skeleton;

	public WaveRightSegment1(Skeleton skeleton) {
		this.skeleton = skeleton;
	}

	@Override
	public GestureResult checkPose(UserData user) {
		// hand above head
		boolean condition1 = (skeleton.getPositionY(user, JointType.RIGHT_HAND) > skeleton.getPositionY(user,
				JointType.HEAD));
		// hand right of elbow
		boolean condition2 = (skeleton.getPositionX(user, JointType.RIGHT_HAND) > skeleton.getPositionX(user,
				JointType.RIGHT_ELBOW));

		if (condition1) {
			if (condition2) {
				return GestureResult.SUCCEED;
			}
			return GestureResult.FAIL;
		}
		return GestureResult.FAIL;
	}

}

class WaveRightSegment2 implements Pose {
	private Skeleton skeleton;

	public WaveRightSegment2(Skeleton skeleton) {
		this.skeleton = skeleton;
	}

	@Override
	public GestureResult checkPose(UserData user) {
		// hand above head
		boolean condition1 = (skeleton.getPositionY(user, JointType.RIGHT_HAND) > skeleton.getPositionY(user,
				JointType.HEAD));
		// hand left of elbow
		boolean condition2 = (skeleton.getPositionX(user, JointType.RIGHT_HAND) < skeleton.getPositionX(user,
				JointType.RIGHT_ELBOW));

		if (condition1) {
			if (condition2) {
				return GestureResult.SUCCEED;
			}
			return GestureResult.FAIL;
		}
		return GestureResult.FAIL;
	}

}
