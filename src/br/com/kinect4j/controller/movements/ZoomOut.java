package br.com.kinect4j.controller.movements;

import com.primesense.nite.JointType;
import com.primesense.nite.UserData;

import br.com.kinect4j.controller.LazyGestureSegment;
import br.com.kinect4j.engine.core.GestureResult;
import br.com.kinect4j.engine.core.Pose;
import br.com.kinect4j.engine.core.Skeleton;
import br.com.kinect4j.engine.core.Skeleton.Operator;

public class ZoomOut {
	private Pose segment1;
	private Pose segment2;
	private Pose segmento3;
	private Pose[] segments;

	public ZoomOut(Skeleton skeleton) {
		segment1 = new ZoomOutSegment1(skeleton);
		segment2 = new ZoomOutSegment2(skeleton);
		segmento3 = new ZoomOutSegment3(skeleton);
		segments = new Pose[] { segment1, segment2, segmento3 };
	}

	public Pose[] getSegments() {
		return segments;
	}
}

// Segment 1
class ZoomOutSegment1 extends LazyGestureSegment {

	protected ZoomOutSegment1(Skeleton skeleton) {
		super(skeleton);
	}

	@Override
	public GestureResult checkPose(UserData user) {
		// Right and Left Hand in front of Shoulders
		boolean condition1 = skeleton.comparePositionZ(user, JointType.LEFT_HAND, Operator.LESS_THAN,
				JointType.LEFT_ELBOW)
				&& skeleton.comparePositionZ(user, JointType.RIGHT_HAND, Operator.LESS_THAN, JointType.RIGHT_ELBOW);

		// Right and Left hand below shoulder height but above hip height
		boolean condition2 = skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.LESS_THAN, JointType.HEAD)
				&& skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.GREATER_THAN, JointType.TORSO)
				&& skeleton.comparePositionY(user, JointType.LEFT_HAND, Operator.LESS_THAN, JointType.HEAD)
				&& skeleton.comparePositionY(user, JointType.LEFT_HAND, Operator.GREATER_THAN, JointType.TORSO);

		// Hands outside elbows
		boolean condition3 = skeleton.comparePositionX(user, JointType.RIGHT_HAND, Operator.GREATER_THAN,
				JointType.RIGHT_ELBOW)
				&& skeleton.comparePositionX(user, JointType.LEFT_HAND, Operator.LESS_THAN, JointType.LEFT_ELBOW);

		return super.checkCondition(condition1, condition2, condition3);
	}

}

// Segment 2

class ZoomOutSegment2 extends LazyGestureSegment {

	protected ZoomOutSegment2(Skeleton skeleton) {
		super(skeleton);
	}

	@Override
	public GestureResult checkPose(UserData user) {
		// Right and Left Hand in front of Shoulders
		boolean condition1 = skeleton.comparePositionZ(user, JointType.LEFT_HAND, Operator.LESS_THAN,
				JointType.LEFT_ELBOW)
				&& skeleton.comparePositionZ(user, JointType.RIGHT_HAND, Operator.LESS_THAN, JointType.RIGHT_ELBOW);

		// Hands between head and hip
		boolean condition2 = skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.LESS_THAN, JointType.HEAD)
				&& skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.GREATER_THAN, JointType.TORSO)
				&& skeleton.comparePositionY(user, JointType.LEFT_HAND, Operator.LESS_THAN, JointType.HEAD)
				&& skeleton.comparePositionY(user, JointType.LEFT_HAND, Operator.GREATER_THAN, JointType.TORSO);

		// Hands outside shoulders
		boolean condition3 = skeleton.comparePositionX(user, JointType.RIGHT_HAND, Operator.GREATER_THAN,
				JointType.RIGHT_SHOULDER)
				&& skeleton.comparePositionX(user, JointType.LEFT_HAND, Operator.LESS_THAN, JointType.LEFT_SHOULDER);

		return super.checkCondition(condition1, condition2, condition3);
	}

}

// Segment3
class ZoomOutSegment3 extends LazyGestureSegment {

	protected ZoomOutSegment3(Skeleton skeleton) {
		super(skeleton);
	}

	@Override
	public GestureResult checkPose(UserData user) {
		// Right and Left Hand in front of Shoulders
		boolean condition1 = skeleton.comparePositionZ(user, JointType.LEFT_HAND, Operator.LESS_THAN,
				JointType.LEFT_ELBOW)
				&& skeleton.comparePositionZ(user, JointType.RIGHT_HAND, Operator.LESS_THAN, JointType.RIGHT_ELBOW);

		// Hands between head and hip
		boolean condition2 = skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.LESS_THAN, JointType.HEAD)
				&& skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.GREATER_THAN, JointType.TORSO)
				&& skeleton.comparePositionY(user, JointType.LEFT_HAND, Operator.LESS_THAN, JointType.HEAD)
				&& skeleton.comparePositionY(user, JointType.LEFT_HAND, Operator.GREATER_THAN, JointType.TORSO);

		// Hands between shoulders
		boolean condition3 = skeleton.comparePositionX(user, JointType.RIGHT_HAND, Operator.LESS_THAN,
				JointType.RIGHT_SHOULDER)
				&& skeleton.comparePositionX(user, JointType.RIGHT_HAND, Operator.GREATER_THAN, JointType.LEFT_SHOULDER)
				&& skeleton.comparePositionX(user, JointType.LEFT_HAND, Operator.GREATER_THAN, JointType.LEFT_SHOULDER)
				&& skeleton.comparePositionX(user, JointType.LEFT_HAND, Operator.LESS_THAN, JointType.RIGHT_SHOULDER);

		return super.checkCondition(condition1, condition2, condition3);
	}

}