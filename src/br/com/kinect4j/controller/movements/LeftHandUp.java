package br.com.kinect4j.controller.movements;

import com.primesense.nite.JointType;
import com.primesense.nite.UserData;

import br.com.kinect4j.engine.core.GestureResult;
import br.com.kinect4j.engine.core.JointRelation;
import br.com.kinect4j.engine.core.Pose;
import br.com.kinect4j.engine.core.Skeleton;

public class LeftHandUp implements Pose {
	private Skeleton skeleton;

	public LeftHandUp(Skeleton skeleton) {
		this.skeleton = skeleton;
	}

	@Override
	public GestureResult checkPose(UserData user) {
		boolean leftUp = skeleton.checkSkeletonPosition(user, JointType.LEFT_HAND, JointRelation.ABOVE, JointType.HEAD);
		return (leftUp) ? GestureResult.SUCCEED : GestureResult.FAIL;
	}
}
