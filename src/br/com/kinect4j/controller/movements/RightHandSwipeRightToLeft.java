package br.com.kinect4j.controller.movements;

import com.primesense.nite.JointType;
import com.primesense.nite.UserData;

import br.com.kinect4j.controller.LazyGestureSegment;
import br.com.kinect4j.engine.core.GestureResult;
import br.com.kinect4j.engine.core.Pose;
import br.com.kinect4j.engine.core.Skeleton;
import br.com.kinect4j.engine.core.Skeleton.Operator;

public class RightHandSwipeRightToLeft {
	private Pose segment1;
	private Pose segment2;
	private Pose segmento3;
	private Pose[] segments;

	public RightHandSwipeRightToLeft(Skeleton skeleton) {
		segment1 = new SwipeLeftSegment1(skeleton);
		segment2 = new SwipeLeftSegment2(skeleton);
		segmento3 = new SwipeLeftSegment3(skeleton);
		segments = new Pose[] { segment1, segment2, segmento3 };
	}

	public Pose[] getSegments() {
		return segments;
	}

}

// Segment 1

class SwipeLeftSegment1 extends LazyGestureSegment {
	
	protected SwipeLeftSegment1(Skeleton skeleton) {
		super(skeleton);
		
	}

	@Override
	public GestureResult checkPose(UserData user) {
		// right hand in front of right shoulder
		boolean condition1 = (skeleton.comparePositionZ(user, JointType.RIGHT_HAND, Operator.LESS_THAN,
				JointType.RIGHT_ELBOW))
				&& skeleton.comparePositionY(user, JointType.LEFT_HAND, Operator.LESS_THAN, JointType.NECK);

		// right hand below shoulder height but above hip height
		boolean condition2 = (skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.LESS_THAN, JointType.HEAD))
				&& skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.GREATER_THAN, JointType.TORSO);

		// right hand right of right shoulder
		boolean condition3 = (skeleton.comparePositionX(user, JointType.RIGHT_HAND, Operator.GREATER_THAN,
				JointType.RIGHT_SHOULDER));

		return super.checkCondition(condition1, condition2, condition3);
	}

}

// Segment 2

class SwipeLeftSegment2 extends LazyGestureSegment {

	protected SwipeLeftSegment2(Skeleton skeleton) {
		super(skeleton);
	}

	@Override
	public GestureResult checkPose(UserData user) {
		// right hand in front of right shoulder
		boolean condition1 = (skeleton.comparePositionZ(user, JointType.RIGHT_HAND, Operator.LESS_THAN,
				JointType.RIGHT_ELBOW))
				&& skeleton.comparePositionY(user, JointType.LEFT_HAND, Operator.LESS_THAN, JointType.NECK);

		// right hand below shoulder height but above hip height
		boolean condition2 = (skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.LESS_THAN, JointType.HEAD))
				&& skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.GREATER_THAN, JointType.TORSO);

		// right hand left of right shoulder & right of left shoulder
		boolean condition3 = (skeleton.comparePositionX(user, JointType.RIGHT_HAND, Operator.LESS_THAN,
				JointType.RIGHT_SHOULDER)
				&& skeleton.comparePositionX(user, JointType.RIGHT_HAND, Operator.GREATER_THAN,
						JointType.LEFT_SHOULDER));

		return super.checkCondition(condition1, condition2, condition3);
	}

}

// Segment 3

class SwipeLeftSegment3 extends LazyGestureSegment {

	protected SwipeLeftSegment3(Skeleton skeleton) {
		super(skeleton);
	}

	@Override
	public GestureResult checkPose(UserData user) {
		// right hand in front of right shoulder
		boolean condition1 = (skeleton.comparePositionZ(user, JointType.RIGHT_HAND, Operator.LESS_THAN,
				JointType.RIGHT_ELBOW))
				&& skeleton.comparePositionY(user, JointType.LEFT_HAND, Operator.LESS_THAN, JointType.NECK);

		// right hand below shoulder height but above hip height
		boolean condition2 = (skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.LESS_THAN, JointType.HEAD))
				&& skeleton.comparePositionY(user, JointType.RIGHT_HAND, Operator.GREATER_THAN, JointType.TORSO);

		// right hand left of center hip
		boolean condition3 = (skeleton.comparePositionX(user, JointType.RIGHT_HAND, Operator.LESS_THAN,
				JointType.LEFT_SHOULDER));

		return super.checkCondition(condition1, condition2, condition3);
	}

}