package br.com.kinect4j.device;

import java.util.ArrayList;
import java.util.List;

import org.openni.Device;
import org.openni.DeviceInfo;
import org.openni.OpenNI;

import com.primesense.nite.NiTE;

public class DeviceConfig {
	private boolean initializeAPI = false;
	private List<DeviceInfo> devicesInfo;
	private List<Device> devicesOpenned;
	// singleton
	private static DeviceConfig uniqueInstance;

	private DeviceConfig() {
		devicesOpenned = new ArrayList<>();
		initializeAPI();
		enumerateDevices();
	}

	public static DeviceConfig getInstance() {
		if (uniqueInstance == null) {
			uniqueInstance = new DeviceConfig();
		}
		return uniqueInstance;
	}

	/**
	 * initialize OpenNI and NiTE
	 */
	private void initializeAPI() {
		try {
			OpenNI.initialize();
			NiTE.initialize();
			initializeAPI = true;
		} catch (Exception e) {
			// do something here!
		}
	}

	/**
	 * get list of devices connected
	 */
	private void enumerateDevices() {
		if (!initializeAPI) {
			devicesInfo = new ArrayList<>();
		}

		devicesInfo = OpenNI.enumerateDevices();
	}

	/**
	 * check if is empty
	 */

	public boolean isDeviceConnected() {
		if (!initializeAPI || devicesInfo.size() == 0) {
			return false;
		}
		return true;
	}

	/**
	 * get first device connected
	 * 
	 * @return
	 */

	public void startFirstDevice() {
		if (isDeviceConnected()) {
			return;
		}
		Device device = Device.open(devicesInfo.get(0).getUri());
		devicesOpenned.add(device);
	}

	public void startDevice(int index) {
		if (isDeviceConnected()) {
			return;
		}
		if (index < 0 || index >= devicesInfo.size() || devicesInfo.size() == 1) {
			startFirstDevice();
		} else {
			Device device = Device.open(devicesInfo.get(index).getUri());
			devicesOpenned.add(device);
		}
	}

	public List<Device> getDevicesOpenned() {
		return devicesOpenned;
	}

	public List<DeviceInfo> getDevicesInfo() {
		return devicesInfo;
	}
}
