package br.com.kinect4j.view;

public enum UserTrackingConfig {
	SYNCHRONOUS, ASYNCHRONOUS;
}
