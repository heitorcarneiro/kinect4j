package br.com.kinect4j.view;

import java.awt.Component;
import java.awt.Graphics;
import java.util.List;

import org.openni.VideoFrameRef;

import com.primesense.nite.SkeletonState;
import com.primesense.nite.UserData;
import com.primesense.nite.UserTracker;
import com.primesense.nite.UserTrackerFrameRef;

import br.com.kinect4j.engine.core.DepthPixels;
import br.com.kinect4j.engine.core.Draw;
import br.com.kinect4j.engine.core.Scenario;
import br.com.kinect4j.engine.core.Skeleton;
import br.com.kinect4j.engine.defaultcore.DefaultDraw;
import br.com.kinect4j.engine.defaultcore.DefaultScenario;
import br.com.kinect4j.engine.defaultcore.DefaultSkeleton;

public abstract class Kinect4jView extends Component implements UserTracker.NewFrameListener, View, Kinect4jObserver {
	private static final long serialVersionUID = 1L;
	private final UserTracker userTracker;
	private final Scenario scenario;
	protected final Skeleton skeleton;
	private final Draw draw;
	/**
	 * used to create the frames
	 */
	private int[] depthPixels;
	private UserTrackerFrameRef userLastFrame;
	/**
	 * Constructor
	 * 
	 * @param UserTracke
	 *            tracker
	 */
	public Kinect4jView(UserTracker tracker) {
		this.userTracker = tracker;
		this.scenario = new DefaultScenario();
		this.skeleton = new DefaultSkeleton(userTracker);
		this.draw = new DefaultDraw(skeleton);
		/**
		 * Java awt kinect Listener
		 */
		this.userTracker.addNewFrameListener(this);
		/**
		 * user config
		 */
		setup(tracker);
	}
	/**
	 * java.awt.Component
	 */
	@Override
	public final synchronized void paint(Graphics g) {
		if (userLastFrame == null) {
			return;
		}

		int framePosX = 0;
		int framePosY = 0;

		VideoFrameRef depthFrame = userLastFrame.getDepthFrame();

		if (depthFrame != null) {
			int width = depthFrame.getWidth();
			int height = depthFrame.getHeight();
			scenario.drawScenario(g, depthPixels, width, height, this);
			framePosX = scenario.getFramePositionX();
			framePosY = scenario.getFramePositionY();
		}

		for (UserData user : userLastFrame.getUsers()) {
			if (user.getSkeleton().getState() == SkeletonState.TRACKED) {
				draw.drawSkeleton(user, g, framePosX, framePosY);
				tracking(user);
			}
		}
	}

	/**
	 * Components Events
	 */
	@Override
	public final synchronized void onNewFrame(UserTracker tracker) {
		if (userLastFrame != null) {
			userLastFrame.release();
			userLastFrame = null;
		}
		// update userLastFrame
		userLastFrame = userTracker.readFrame();
		// check if any new user detected
		updateUsers(userLastFrame.getUsers());
		// update screen
		updateDepthPixels(userLastFrame.getDepthFrame());
		// repaint Component
		repaint();
	}

	@Override
	public final void updateDepthPixels(VideoFrameRef depthFrame) {
		if (depthFrame != null) {
			DepthPixels pixels = new DepthPixels(depthFrame);
			this.depthPixels = pixels.calculateDepthPixels(userLastFrame, draw);
		}
	}

	/**
	 * Users Events
	 */
	@Override
	public final void updateUsers(List<UserData> users) {
		for (UserData user : users) {
			if (onNewUser(user)) {
				System.out.println("New user detected:" + user.getId());
			} else if (onLostUser(user, users.size())) {
				System.out.println("Lost user:" + user.getId());
			}
		}
	}

	@Override
	public final boolean onNewUser(UserData user) {
		boolean condition = user.isNew();
		if (condition) {
			// start skeleton tracking
			userTracker.startSkeletonTracking(user.getId());
		}
		return condition;

	}

	@Override
	public final boolean onLostUser(UserData user, int usersListSize) {
		boolean condition = user.isLost() && usersListSize > 1;
		if (condition) {
			// stop skeleton tracking
			userTracker.stopSkeletonTracking(user.getId());
		}
		return condition;
	}

	
	@Override
	public final void asyncUserTracking(UserData user) {
		new Thread(() -> {
			userTracking(user);
		}).start();
	}
	
	@Override
	public final void tracking(UserData user) {
		if(getUserTrackingConfig() == UserTrackingConfig.ASYNCHRONOUS){
			asyncUserTracking(user);
		}
		else if(getUserTrackingConfig() == UserTrackingConfig.SYNCHRONOUS){
			userTracking(user);
		}
	}
	
}