package br.com.kinect4j.view;

import java.util.List;

import org.openni.VideoFrameRef;

import com.primesense.nite.UserData;
import com.primesense.nite.UserTracker;

public interface View {
	void setup(UserTracker userTracker);

	void updateDepthPixels(VideoFrameRef depthFrame);

	void updateUsers(List<UserData> users);

	boolean onNewUser(UserData user);

	boolean onLostUser(UserData user, int usersListSize);

	void tracking(UserData user);
	
	UserTrackingConfig getUserTrackingConfig();

	void userTracking(UserData user);

	void asyncUserTracking(UserData user);
}
