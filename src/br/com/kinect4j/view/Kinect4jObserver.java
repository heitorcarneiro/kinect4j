package br.com.kinect4j.view;

public interface Kinect4jObserver {

	<T extends Enum<T>> void updateGestureName(T name);

	<T extends Enum<T>> void updatePoseName(T name);
}
