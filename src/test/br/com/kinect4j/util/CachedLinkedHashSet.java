package test.br.com.kinect4j.util;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class CachedLinkedHashSet<E> extends LinkedHashSet<E> {
	private static final long serialVersionUID = 1L;
	private E last = null;

	@Override
	public boolean add(E e) {
		synchronized (this) {
			last = e;
		}
		return super.add(e);
	}

	public E getFirst() {
		E first = null;
		Iterator<E> i = super.iterator();
		while (i.hasNext()) {
			first = i.next();
			break;
		}
		return first;
	}

	public E getLast() {
		return last;
	}

	public E getNext(E actual) {
		E next = null;
		int current = 0;
		if (super.contains(actual)) {
			E first = getFirst();
			if (actual.equals(getLast()))
			// if is the last element
			{
				// do nothing!
			} else if (actual.equals(first))
			// check if is the first
			{
				current++;
				Iterator<E> i = super.iterator();
				while (i.hasNext()) {
					E currentElement = i.next();
					if(current == 1){
						next = currentElement;
						break;
					}
				
				}
				
			} else
			// start search in the set
			{
				/**
				 * TODO
				 */
			}
		}
		return next;
	}

}
