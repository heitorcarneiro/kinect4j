package test.br.com.kinect4j.application.controller;

import br.com.kinect4j.controller.Controller;
import test.br.com.kinect4j.application.ApplicationView;

public class SwipeRightToLeftController implements Controller {
	private ApplicationView view;

	public SwipeRightToLeftController(ApplicationView view) {
		this.view = view;
	}

	@Override
	public void kinectActionPerformed() {
		System.out.println("Swipe Right to Left!");
	}

}
