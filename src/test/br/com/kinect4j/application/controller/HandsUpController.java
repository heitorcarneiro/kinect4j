package test.br.com.kinect4j.application.controller;

import br.com.kinect4j.controller.Controller;
import test.br.com.kinect4j.application.ApplicationView;

public class HandsUpController implements Controller {
	private ApplicationView view;

	public HandsUpController(ApplicationView view) {
		this.view = view;
	}

	@Override
	public void kinectActionPerformed() {
		System.err.println("Hands UP");
		view.switchGestureTraking();
	}

}
