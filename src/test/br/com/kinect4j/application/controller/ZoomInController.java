package test.br.com.kinect4j.application.controller;

import br.com.kinect4j.controller.Controller;
import test.br.com.kinect4j.application.ApplicationView;

public class ZoomInController implements Controller {
	private ApplicationView view;

	public ZoomInController(ApplicationView view) {
		this.view = view;
	}

	@Override
	public void kinectActionPerformed() {
		System.out.println("Zoom In!");
	}

}