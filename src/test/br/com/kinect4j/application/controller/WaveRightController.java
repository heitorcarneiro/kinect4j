package test.br.com.kinect4j.application.controller;

import br.com.kinect4j.controller.Controller;
import test.br.com.kinect4j.application.ApplicationView;

public class WaveRightController implements Controller {
	private ApplicationView view;

	public WaveRightController(ApplicationView view) {
		this.view = view;
	}

	@Override
	public void kinectActionPerformed() {
		System.out.println("Wave Right!");
	}

}