package test.java.experiences;

import test.br.com.kinect4j.util.CachedLinkedHashSet;

public class C extends B{
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		A<C> a = new A<C>(C.class);
		System.out.println(a.getNomeMovimento());
		
		CachedLinkedHashSet<String> cachedLinkedHashSet = new CachedLinkedHashSet<>();
		
		for(Integer i = 0; i < 100; i ++){
			cachedLinkedHashSet.add(i.toString());
			System.out.println("First:" +cachedLinkedHashSet.getFirst());
			System.out.println("Last:" +cachedLinkedHashSet.getLast());
		}
	}
}
