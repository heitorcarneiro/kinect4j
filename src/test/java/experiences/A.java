package test.java.experiences;

import test.br.com.kinect4j.util.ReflectionFactory;

public class A<T extends B> {
	private T movimento;
	
	public A(Class<T> m) {
		this.movimento = ReflectionFactory.getInstance(m);
	}
		
	public String getNomeMovimento() {
		return movimento.getNome();
	}
}
