package test.java.experiences;

public class RandomMain {
	enum RandomEnum{
		ZOOM_IN, ZOOM_OUT;
	}
	public static <T extends Enum<T>> void notifyGetureName(T name){
		System.out.println(name);
	}
	
	public static void notifyGetureName(String name){
		System.out.println(name);
	}

	public static void main(String[] args) {
		notifyGetureName(RandomEnum.ZOOM_IN);
		notifyGetureName("UHUU");
	}

}
