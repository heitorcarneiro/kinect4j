package test.java.experiences;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.primesense.nite.UserTracker;

import br.com.kinect4j.device.DeviceConfig;

public class SkeletonTrackerApplication {
	private SkeletonTracker skeletonTracker;
	private JFrame frame;
	private boolean isRunning = true;

	public SkeletonTrackerApplication(UserTracker userTracker) {
		this.skeletonTracker = new SkeletonTracker(userTracker);
		frame = new JFrame("Skeleton Tracker");
		// register to closing event
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				isRunning = false;
			}
		});
		frame.setSize(800, 600);
		frame.add("Center", skeletonTracker);
		frame.setVisible(true);
	}

	void run() {
		while (isRunning) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.frame.dispose();
	}

	public static void main(String[] args) {	
		// Test DeviceConfig
		
		DeviceConfig kinect = DeviceConfig.getInstance();
		
		if(!kinect.isDeviceConnected()){
			JOptionPane.showMessageDialog(null, "No device is connected", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		kinect.startFirstDevice();
		
		
		UserTracker userTracker = UserTracker.create();

		SkeletonTrackerApplication app = new SkeletonTrackerApplication(userTracker);
		app.run();
	}
}
