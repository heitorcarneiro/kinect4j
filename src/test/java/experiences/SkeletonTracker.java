package test.java.experiences;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.openni.VideoFrameRef;

import com.primesense.nite.JointType;
import com.primesense.nite.Point2D;
import com.primesense.nite.SkeletonJoint;
import com.primesense.nite.SkeletonState;
import com.primesense.nite.UserData;
import com.primesense.nite.UserTracker;
import com.primesense.nite.UserTrackerFrameRef;

public class SkeletonTracker extends Component implements UserTracker.NewFrameListener {
	private static final long serialVersionUID = 1L;
	private float histogram[];
	private int[] depthPixels;
	private UserTracker userTracker;
	private UserTrackerFrameRef userLastFrame;
	private BufferedImage bufferedImage;
	private int[] userSkeletonColors;

	public SkeletonTracker(UserTracker tracker) {
		userTracker = tracker;
		userTracker.addNewFrameListener(this);
		userSkeletonColors = new int[] { 0xFFFF0000, 0xFF00FF00, 0xFF0000FF, 0xFFFFFF00, 0xFFFF00FF, 0xFF00FFFF };
	}

	/**
	 * java awt Component
	 */
	@Override
	public synchronized void paint(Graphics g) {
		if (userLastFrame == null) {
			return;
		}

		int framePosX = 0;
		int framePosY = 0;

		VideoFrameRef depthFrame = userLastFrame.getDepthFrame();
		if (depthFrame != null) {
			int width = depthFrame.getWidth();
			int height = depthFrame.getHeight();

			// make sure we have enough room
			if (bufferedImage == null || bufferedImage.getWidth() != width || bufferedImage.getHeight() != height) {
				bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			}

			bufferedImage.setRGB(0, 0, width, height, depthPixels, 0, width);

			framePosX = (getWidth() - width) / 2;
			framePosY = (getHeight() - height) / 2;

			g.drawImage(bufferedImage, framePosX, framePosY, null);
		}

		for (UserData user : userLastFrame.getUsers()) {
			if (user.getSkeleton().getState() == SkeletonState.TRACKED) {
				printStatus(user);
				drawSkeleton(user, g, framePosX, framePosY);
			}
		}
	}

	private void printStatus(UserData user) {
		Point2D<Float> point = getPosition2D(getSkeletonJoint(user, JointType.RIGHT_HAND));
		StringBuffer str = new StringBuffer();
		str.append("x:").append(point.getX()).append(" y:").append(point.getY());
		System.out.println(str.toString());
	}

	private SkeletonJoint getSkeletonJoint(UserData user, JointType type) {
		return user.getSkeleton().getJoint(type);
	}

	private Point2D<Float> getPosition2D(SkeletonJoint skeletonJoint) {
		return this.userTracker.convertJointCoordinatesToDepth(skeletonJoint.getPosition());
	}

	private Color getColor(UserData user) {
		return new Color(userSkeletonColors[(user.getId() + 1) % userSkeletonColors.length]);
	}

	private void drawLine(Graphics g, int framePosX, int framePosY, Point2D<Float> fromPosition,
			Point2D<Float> toPosition) {
		int x1 = framePosX + fromPosition.getX().intValue();
		int y1 = framePosY + fromPosition.getY().intValue();
		int x2 = framePosX + toPosition.getX().intValue();
		int y2 = framePosY + toPosition.getY().intValue();

		g.drawLine(x1, y1, x2, y2);
	}

	private boolean validPositionConfidence(SkeletonJoint fromJoint, SkeletonJoint toJoint) {
		return (fromJoint.getPositionConfidence() == 0.0 || toJoint.getPositionConfidence() == 0.0);
	}

	private void drawLimb(Graphics g, int framePosX, int framePosY, UserData user, JointType from, JointType to) {
		SkeletonJoint fromJoint = getSkeletonJoint(user, from);
		SkeletonJoint toJoint = getSkeletonJoint(user, to);

		if (validPositionConfidence(fromJoint, toJoint)) {
			return;
		}

		Point2D<Float> fromPosition = getPosition2D(fromJoint);
		Point2D<Float> toPosition = getPosition2D(toJoint);

		// draw it in another color than the use color
		g.setColor(getColor(user));
		drawLine(g, framePosX, framePosY, fromPosition, toPosition);
	}

	private void drawSkeleton(UserData user, Graphics g, int framePosX, int framePosY) {
		drawLimb(g, framePosX, framePosY, user, JointType.HEAD, JointType.NECK);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_SHOULDER, JointType.LEFT_ELBOW);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_ELBOW, JointType.LEFT_HAND);
		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_SHOULDER, JointType.RIGHT_ELBOW);
		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_ELBOW, JointType.RIGHT_HAND);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_SHOULDER, JointType.RIGHT_SHOULDER);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_SHOULDER, JointType.TORSO);
		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_SHOULDER, JointType.TORSO);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_HIP, JointType.TORSO);
		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_HIP, JointType.TORSO);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_HIP, JointType.RIGHT_HIP);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_HIP, JointType.LEFT_KNEE);
		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_KNEE, JointType.LEFT_FOOT);
		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_HIP, JointType.RIGHT_KNEE);
		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_KNEE, JointType.RIGHT_FOOT);
	}

	/**
	 * Listner UserTracker
	 */
	@Override
	public synchronized void onNewFrame(UserTracker tracker) {
		if (userLastFrame != null) {
			userLastFrame.release();
			userLastFrame = null;
		}

		userLastFrame = userTracker.readFrame();

		// check if any new user detected
		for (UserData user : userLastFrame.getUsers()) {
			if (user.isNew()) {
				// start skeleton tracking
				userTracker.startSkeletonTracking(user.getId());
			}
		}

		VideoFrameRef depthFrame = userLastFrame.getDepthFrame();

		if (depthFrame != null) {
			ByteBuffer frameData = depthFrame.getData().order(ByteOrder.LITTLE_ENDIAN);
			ByteBuffer usersFrame = userLastFrame.getUserMap().getPixels().order(ByteOrder.LITTLE_ENDIAN);

			// make sure we have enough room
			if (depthPixels == null || depthPixels.length < depthFrame.getWidth() * depthFrame.getHeight()) {
				depthPixels = new int[depthFrame.getWidth() * depthFrame.getHeight()];
			}

			calculateHistogram(frameData);
			frameData.rewind();

			int pos = 0;
			while (frameData.remaining() > 0) {
				short depth = frameData.getShort();
				short userId = usersFrame.getShort();
				short pixel = (short) histogram[depth];
				int color = 0xFFFFFFFF;
				if (userId > 0) {
					color = userSkeletonColors[userId % userSkeletonColors.length];
				}

				depthPixels[pos] = color & (0xFF000000 | (pixel << 16) | (pixel << 8) | pixel);
				pos++;
			}
		}

		repaint();
	}

	private void calculateHistogram(ByteBuffer depthBuffer) {
		// make sure we have enough room
		if (histogram == null) {
			histogram = new float[10000];
		}

		// reset
		for (int i = 0; i < histogram.length; ++i)
			histogram[i] = 0;

		int points = 0;
		while (depthBuffer.remaining() > 0) {
			int depth = depthBuffer.getShort() & 0xFFFF;
			if (depth != 0) {
				histogram[depth]++;
				points++;
			}
		}

		for (int i = 1; i < histogram.length; i++) {
			histogram[i] += histogram[i - 1];
		}

		if (points > 0) {
			for (int i = 1; i < histogram.length; i++) {
				histogram[i] = (int) (256 * (1.0f - (histogram[i] / (float) points)));
			}
		}
	}
}
